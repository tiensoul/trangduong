<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'trangchu',
    'uses' => 'PageController@getIndex'
]);

Route::get('chi-tiet/{id}/{id_type?}', [
	'as' => 'chitiet',
	'uses' => 'pageController@getChitiet'
]);

// Route::post('tim-kiem', [
// 	'as' => 'timkiem',
// 	'uses' => 'pageController@postTimKiem'
// ]);

Route::get('tim-kiem', [
	'as' => 'timkiem',
	'uses' => 'pageController@getTimKiem'
]);

Route::get('loai-sp/{type}/{id_type?}', [
	'as' => 'loaisp',
	'uses' => 'pageController@getLoaiSP'
]);

Route::get('loai-sp-con/{id}', [
	'as' => 'loaispcon',
	'uses' => 'pageController@getLoaiSPCon'
]);

Route::get('thanh-toan', function () {
    return view('pages/thanhtoan');
});

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getGioHang'
]);

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getThanhtoan'
]);

Route::post('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@postThanhToan'
]);

Route::get('cartRemoveOne/{id}', [
	'as' => 'cartRemoveOne',
	'uses' => 'pageController@getXoaMotSanPham'
]);


Route::get('tai-khoan', [
	'as' => 'taikhoan',
	'uses' => 'pageController@getDangKy'
]);

Route::post('tai-khoan', [
	'as' => 'taikhoan',
	'uses' => 'pageController@postDangKy'
]);

Route::get('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@getDangNhap'
]);

Route::post('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@postDangNhap'
]);

Route::get('dang-xuat', [
	'as' => 'dangxuat',
	'uses' => 'PageController@getDangXuat'
]);

Route::get('cau-hinh', [
	'as' => 'cauhinh',
	'uses' => 'PageController@getCauHinh'
]);

Route::post('thay-logo', [
	'as' => 'thaylogo',
	'uses' => 'PageController@postThayLogo'
]);

Route::post('thay-slide', [
	'as' => 'thayslide',
	'uses' => 'PageController@postThaySlide'
]);

Route::post('cap-nhat-khach-hang', [
	'as' => 'capnhatKH',
	'uses' => 'PageController@postCapNhatKH'
]);

Route::post('xoa-khach-hang', [
	'as' => 'xoaKH',
	'uses' => 'PageController@postXoaKH'
]);

Route::post('xoa-thanh-vien', [
	'as' => 'xoaTV',
	'uses' => 'PageController@postXoaTv'
]);


Route::post('thay-san-pham', [
	'as' => 'thaysanpham',
	'uses' => 'PageController@postThaySP'
]);

Route::get('gioi-thieu', [
	'as' => 'gioithieu',
	'uses' => 'PageController@getGioiThieu'
]);

Route::get('san-pham-khuyen-mai', [
	'as' => 'sanphamkhuyenmai',
	'uses' => 'PageController@getSanPhamKhuyenMai'
]);

Route::get('huong-dan-mua-hang', [
	'as' => 'huongdanmuahang',
	'uses' => 'PageController@getHuongDanMuaHang'
]);


Route::get('quy-dinh-doi-tra', [
	'as' => 'quydinhdoitra',
	'uses' => 'PageController@getQuyDinhDoiTra'
]);


Route::get('cach-mua-hang', function () {
    return view('pages/cachmuahang');
});


//cart
Route::get('cartAdd/{id}', [
	'as' => 'cartAdd',
	'uses' => 'PageController@getThemSanPham'
]);

Route::get('buyNow/{id}', [
	'as' => 'buyNow',
	'uses' => 'PageController@getMuaNgay'
]);

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getGioHang'
]);

Route::get('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@getThanhtoan'
]);

Route::post('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@postThanhtoan'
]);

Route::get('cartRemoveAll/{id}', [
	'as' => 'cartRemoveAll',
	'uses' => 'pageController@getXoaTatCaSP'
]);

//admin
Route::get('admin/dangxuat', [
	'as' => 'dangxuatadmin',
	'uses' => 'PageController@getDangXuatAdmin'
]);

//quan tri
Route::get('admin/login', [
	'as' => 'login',
	'uses' => 'PageController@getLoginAdmin'
]);

Route::post('admin/login', [
	'as' => 'login',
	'uses' => 'PageController@postLoginAdmin'
]);

Route::get('admin/index', [
	'as' => 'indexAdmin',
	'uses' => 'PageController@getIndexAdmin'
]);

Route::get('admin/sua-san-pham/{id}', [
	'as' => 'suasanpham',
	'uses' => 'PageController@getSuaSanPham'
]);

// Route::post('admin/sua-san-pham', [
// 	'as' => 'suasanpham',
// 	'uses' => 'PageController@postSuaSanPham'
// ]);

Route::get('admin/sanpham', [
	'as' => 'sanpham',
	'uses' => 'PageController@getSanPham'
]);

Route::post('admin/sua-san-pham/{id}', [
	'as' => 'suasanpham',
	'uses' => 'PageController@postCapNhat'
]);

Route::post('admin/themsanpham', [
	'as' => 'themsanpham',
	'uses' => 'PageController@postThemSanPhamAdmin'
]);

Route::get('admin/themsanpham', [
	'as' => 'themsanpham',
	'uses' => 'PageController@getThemSanPhamAdmin'
]);

Route::post('admin/xoasanpham', [
	'as' => 'xoasanpham',
	'uses' => 'PageController@postXoaSanPham'
]);

Route::get('admin/slide', [
	'as' => 'slide',
	'uses' => 'PageController@getSlide'
]);

Route::post('admin/capnhatslide', [
	'as' => 'capnhatslide',
	'uses' => 'PageController@postCapNhatSlide'
]);

Route::post('admin/themslide', [
	'as' => 'themslide',
	'uses' => 'PageController@postThemSlide'
]);

Route::post('admin/xoaslide', [
	'as' => 'xoaslide',
	'uses' => 'PageController@postXoaSlide'
]);

Route::get('admin/donhang', [
	'as' => 'donhang',
	'uses' => 'PageController@getDonHang'
]);

Route::post('admin/tiepnhandonhang', [
	'as' => 'tiepnhandonhang',
	'uses' => 'PageController@postTiepNhanDonHang'
]);

Route::post('admin/xulydonhang', [
	'as' => 'xulydonhang',
	'uses' => 'PageController@postXuLyDonHang'
]);

Route::get('admin/khachhang', [
	'as' => 'khachhang',
	'uses' => 'PageController@getKhachHang'
]);

Route::get('admin/thanhvien', [
	'as' => 'thanhvien',
	'uses' => 'PageController@getThanhVien'
]);

Route::get('admin/slide', [
	'as' => 'slide',
	'uses' => 'PageController@getSlide'
]);

Route::post('admin/capnhatslide', [
	'as' => 'capnhatslide',
	'uses' => 'PageController@postCapNhatSlide'
]);

Route::post('admin/themslide', [
	'as' => 'themslide',
	'uses' => 'PageController@postThemSlide'
]);

Route::post('admin/xoaslide', [
	'as' => 'xoaslide',
	'uses' => 'PageController@postXoaSlide'
]);

Route::get('admin/donhang', [
	'as' => 'donhang',
	'uses' => 'PageController@getDonHang'
]);

Route::post('admin/tiepnhandonhang', [
	'as' => 'tiepnhandonhang',
	'uses' => 'PageController@postTiepNhanDonHang'
]);

Route::post('admin/xulydonhang', [
	'as' => 'xulydonhang',
	'uses' => 'PageController@postXuLyDonHang'
]);

Route::post('admin/memberedit', [
	'as' => 'capnhatmember',
	'uses' => 'PageController@postMemberEdit'
]);

Route::get('admin/danhmuc', [
	'as' => 'danhmuc',
	'uses' => 'PageController@getDanhMuc'
]);

Route::post('admin/danhmuc', [
	'as' => 'danhmuc',
	'uses' => 'PageController@postDanhMuc'
]);

Route::post('admin/capnhatlsp', [
	'as' => 'capnhatlsp',
	'uses' => 'PageController@postCapNhatLSP'
]);

Route::post('admin/xoalsp', [
	'as' => 'xoalsp',
	'uses' => 'PageController@postXoaLSP'
]);


