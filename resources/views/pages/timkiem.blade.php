@extends('pages/master')
@section('content')
</div>
<style>
    .main-page-wrapper {
        margin-top: 0px;
    }
</style>
<!-- MAIN CONTENT AREA -->
<div class="container">
        <div class="row">

            <div class="site-content shop-content-area col-sm-9 col-sm-push-3 content-with-products description-area-before"
                role="main">
                <div class="shop-loop-head">
                    <h4 style="float: left;">Tìm thấy {{ count($search_result) }} sản phẩm cho từ khóa: "{{ $keyshow }}"</h5>
                </div>

                <div class="basel-active-filters">
                </div>

                <div class="basel-shop-loader"></div>


                <div class="products elements-grid basel-products-holder  basel-spacing- products-spacing- pagination-pagination row grid-columns-4"
                    data-min_price="" data-max_price="" data-source="main_loop">

                    @foreach($search_result as $sr)
                    <div class="product-grid-item basel-hover-alt product  col-xs-6 col-sm-4 col-md-3 first  post-100956 type-product status-publish has-post-thumbnail product_cat-cham-soc-body product_cat-cham-soc-chan product_tag-cha-got-de-thuong product_tag-cham-soc-chan product_tag-dung-cu-cha-got-chan first instock shipping-taxable purchasable product-type-simple"
                        data-loop="1" data-id="100956">

                        <div class="product-element-top">
                            @if($sr->promotion_price != 0)
                                <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($sr->promotion_price / $sr->unit_price) * 100), 0) }}%</span></div>
                            @endif
                            <a href="{{ route('chitiet',[$sr->id, $sr->id_type]) }}">
                                <img width="300" height="300"
                                    src="uploads/product/{{ $sr->image }}"
                                    class="lazy lazy-hidden jetpack-lazy-image" alt="" data-lazy-type="image"
                                    data-lazy-src="uploads/product/{{ $sr->image }}"
                                    />
                            </a>
                            <div class="basel-buttons">
                            </div>
                        </div>
                        <h3 class="product-title"><a href="{{ route('chitiet', [$sr->id, $sr->id_type]) }}">{{ $sr->name }}</a></h3>

                        <div class="wrap-price">
                            <div class="wrapp-swap">
                                <div class="swap-elements">

                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                            @if($sr->promotion_price != 0)
                                            <span style="text-decoration: line-through; color: #a52a2a;">
                                                {{ number_format(($sr->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($sr->promotion_price), 0, '.', ',') }}đ
                                            @else
                                                {{ number_format(($sr->unit_price), 0, '.', ',') }}đ
                                            @endif
                                            </span>
                                        </span>
                                    <div class="btn-add">
                                        <a href="{{ route('cartAdd', $sr->id) }}" 
                                            class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                </div>
                            </div>

                        </div>



                    </div>
                    @endforeach
                </div>
                <div class="products-footer">
                    <nav class="woocommerce-pagination">
                        <ul class='page-numbers'>
                                {{ $search_result->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
            <aside class="sidebar-container col-sm-3 col-sm-pull-9 sidebar-left area-sidebar-shop"
                role="complementary">
                <div class="basel-close-sidebar-btn"><span>Close</span></div>
                <div class="sidebar-inner basel-sidebar-scroll">
                    <div class="widget-area basel-sidebar-content">
                        <div id="woocommerce_product_categories-2"
                            class="sidebar-widget woocommerce widget_product_categories">
                            <h5 class="widget-title">Danh mục</h5>
                            <ul class="product-categories">
                                
                                <li class="cat-item cat-item-112 cat-parent current-cat-parent"><a
                                        href="{{ route('trangchu') }}"></a>
                                    <ul class='children'>

                                        @foreach($loaisp as $lsp)
                                        <li class="cat-item cat-item-776 current-cat"><a
                                                href="{{ route('loaisp' , [$lsp->id, $lsp->id]) }}">{{ $lsp->name }}</a>
                                        </li>
                                        @endforeach

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div><!-- .widget-area -->
                </div><!-- .sidebar-inner -->
            </aside><!-- .sidebar-container -->
        </div> <!-- end row -->
    </div> <!-- end container -->
@endsection('content')