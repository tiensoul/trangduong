@extends('pages/master')
@section('content')
<style>
    .navigation-wrap {
        height: 90px;
    }
    .alert {
        padding: 15px;
        background-color: #f44336;
        color: white;
    }

    .alert-success {
        padding: 15px;
        background-color: #4CAF50;
        color: white;
    }

    .alert-danger {
        padding: 15px;
        background-color: #f44336;
        color: white;
    }
      
      .closebtn {
        margin-left: 15px;
        color: white;
        font-weight: bold;
        float: right;
        font-size: 22px;
        line-height: 15px;
        cursor: pointer;
        transition: 0.3s;
      }
      
      .closebtn:hover {
        color: black;
      }
</style>
<div class="main-page-wrapper">

    <div class="page-title page-title-default title-size-small title-design-centered color-scheme-light" style="">
        <div class="container">
            <header class="entry-header">
                <h1 class="entry-title">Tài khoản của tôi</h1>
                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><a href="{{ route('trangchu') }}"
                        rel="v:url" property="v:title">Home</a> » <span class="current">Tài khoản của tôi</span></div>
                <!-- .breadcrumbs -->
            </header><!-- .entry-header -->
        </div>
    </div>

    <!-- MAIN CONTENT AREA -->
    <div class="container">
        <div class="row">


            <div class="site-content" role="main">
                <div class="container">

                    <div class="woocommerce">
                            @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                <strong>Thông báo!</strong>
                                @foreach($errors->all() as $err)
                                    {{ $err }}
                                @endforeach
                            </div>
                            @endif
                
                            @if(Session::has('thongbao'))
                                <div class="alert-success">
                                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                    <strong>Thông báo!</strong>
                                    {{ Session::get('thongbao') }}
                                </div>
                            @endif

                            @if(Session::has('flag'))
							    <div class="alert-{{ Session::get('flag') }}">{{ Session::get('message') }}</div>
						    @endif


                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="basel-registration-page basel-register-tabs">


                            <div class="u-columns col2-set" id="customer_login">

                                <div class="u-column1 col-1 col-login">


                                    <h2>Đăng nhập</h2>

                                    <form method="post" action="{{ route('dangnhap') }}" class="login woocommerce-form woocommerce-form-login ">
                                        @csrf


                                        <p
                                            class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-username">
                                            <label for="username">Tên đang nhập hoặc email&nbsp;<span
                                                    class="required">*</span></label>
                                            <input type="text"
                                                class="woocommerce-Input woocommerce-Input--text input-text"
                                                name="email" id="username" autocomplete="username" value="">
                                        </p>
                                        <p
                                            class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-password">
                                            <label for="password">Mật khẩu&nbsp;<span class="required">*</span></label>
                                            <input class="woocommerce-Input woocommerce-Input--text input-text"
                                                type="password" name="password" id="password"
                                                autocomplete="current-password">
                                        </p>


                                        <p class="form-row">
                                            <input type="hidden" id="woocommerce-login-nonce"
                                                name="woocommerce-login-nonce" value="48c72f59d6"><input type="hidden"
                                                name="_wp_http_referer" value="/tai-khoan"> <button type="submit"
                                                class="button woocommerce-Button" name="login" value="Đăng nhập">Đăng
                                                nhập</button>
                                        </p>

                                        



                                    </form>



                                </div>

                                <div class="u-column2 col-2 col-register">

                                    <h2>Đăng ký</h2>

                                    <form method="post" action="{{ route('taikhoan') }}" class="woocommerce-form woocommerce-form-register register">
                                        @csrf



                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                    <label for="reg_email">Họ và tên khách hàng&nbsp;<span
                                                            class="required">*</span></label>
                                                    <input type="text"
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        name="hoten" id="reg_email" value="{{ old('hoten') }}">
                                                </p>
                                                
        
        
                                                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                    <label for="reg_email">Địa chỉ email&nbsp;<span
                                                            class="required">*</span></label>
                                                    <input type="email"
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        name="email" id="reg_email" autocomplete="email" value="{{ old('email') }}">
                                                </p>

                                                <p class="form-row form-row-wide address-field validate-state"
                                                        id="billing_state_field" data-priority="80"><label
                                                            for="billing_state" class="">Giới tính<span
                                                                class="optional"></span></label><span
                                                            class="woocommerce-input-wrapper"><select
                                                                name="gioitinh" id="billing_state"
                                                                class="state_select  select2-hidden-accessible"
                                                                autocomplete="address-level1" data-placeholder=""
                                                                tabindex="-1" aria-hidden="true">
                                                                <option value="">Chọn giới tính</option>
                                                                <option value="1">Nam
                                                                </option>
                                                                <option value="0">Nữ</option>
                                                            </select>
                                                            </span></p>
        
                                                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                    <label for="reg_email">Số điện thoại&nbsp;<span
                                                            class="required">*</span></label>
                                                    <input type="text"
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        name="sodienthoai" id="reg_email" value="{{ old('sodienthoai') }}">
                                                </p>
        
                                                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                    <label for="reg_email">Địa chỉ nhận hàng&nbsp;<span
                                                            class="required">*</span></label>
                                                    <input type="text"
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        name="diachi" id="reg_email" value="{{ old('diachi') }}">
                                                </p>
        
                                                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                        <label for="reg_password">Mật khẩu&nbsp;<span
                                                                class="required">*</span></label>
                                                        <input type="password"
                                                            class="woocommerce-Input woocommerce-Input--text input-text"
                                                            name="password" id="reg_password" autocomplete="new-password">
                                                    </p>

                                                    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                            <label for="reg_password">Nhập lại mật khẩu&nbsp;<span
                                                                    class="required">*</span></label>
                                                            <input type="password"
                                                                class="woocommerce-Input woocommerce-Input--text input-text"
                                                                name="repassword" id="reg_password" autocomplete="new-password">
                                                        </p>


                                        <div class="woocommerce-privacy-policy-text"></div>
                                        <p class="woocommerce-FormRow form-row">
                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="6e63818fb8"><input
                                                type="hidden" name="_wp_http_referer" value="/tai-khoan"> <button
                                                type="submit" class="woocommerce-Button button" name="register"
                                                value="Đăng ký">Đăng ký</button>
                                        </p>


                                    </form>

                                </div>

                                <div class="col-2 col-register-text">

                                    <span class="register-or">Or</span>

                                    <h2>Hướng dẫn</h2>


                                    <div class="registration-info">Đăng ký cho trang web này cho phép bạn truy cập trạng
                                        thái và lịch sử đơn đặt hàng của mình. Chỉ cần điền vào các trường bên dưới và
                                        chúng tôi sẽ sớm thiết lập tài khoản mới cho bạn. Chúng tôi sẽ chỉ yêu cầu bạn
                                        cung cấp thông tin cần thiết để thực hiện quy trình mua hàng nhanh hơn và dễ
                                        dàng hơn.</div>

                                    <a href="{{ route('taikhoan') }}" class="btn btn-color-black basel-switch-to-register" data-login="Đăng nhập" data-register="Đăng ký tài khoản"
                                        >Đăng ký tài khoản</a>

                                </div>

                            </div>

                        </div><!-- .basel-registration-page -->

                    </div>
                </div>
            </div>

        </div> <!-- end row -->
    </div> <!-- end container -->
</div>
@endsection('content')