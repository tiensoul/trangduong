<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeProduct extends Model
{
    protected $table = 'type_product';

    public function product()
    {
        return $this->hasOne('App/Product', 'id_type', 'id');
    }

    public function subcategory()
    {
        return $this->hasOne('App/SubCategory', 'id', 'id_type');
    }
}
