<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            ['logo_image' => 'logo.png', 'product_sale' => '6', 'product_top' => '6']
        ]);
    }
}
