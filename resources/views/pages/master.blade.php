<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" lang="vi" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="vi" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->

<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="xmlrpc.php">
    <base href="{{ asset('source/fonts/') }}">

	<meta name="theme-color" content="#ec268f" />
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#ec268f" />
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop</title>


	<!-- This site is optimized with the Yoast SEO plugin v9.2.1 - https://yoast.com/wordpress/plugins/seo/ -->
	<meta name="description"
		content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<link rel="canonical" href="{{ route('trangchu') }}" />
	<meta property="og:locale" content="vi_VN" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<meta property="og:description"
		content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<meta property="og:url" content="{{ route('trangchu') }}" />
	<meta property="og:site_name" content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<meta property="og:image:width" content="1601" />
	<meta property="og:image:height" content="481" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:description"
		content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<meta name="twitter:title" content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />
	<meta name="twitter:image" content="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." />

	<link rel='dns-prefetch' href='http://s0.wp.com/' />
	<link rel='dns-prefetch' href='http://s.w.org/' />
	<link rel="alternate" type="application/rss+xml" title="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." href="feed" />
	<link rel="alternate" type="application/rss+xml" title="Shop mỹ phẩm Trang Dương. Cam kết HÀNG CHÍNH HÃNG giá tốt nhất với tất cả thương hiệu tại Shop." href="comments/feed" />
	<link rel='stylesheet' id='wp-block-library-css' href='css/style.min.css' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css' href='css/stylesc721.css?ver=5.1' type='text/css' media='all' />
	<style id='woocommerce-inline-inline-css' type='text/css'>
		.woocommerce form .form-row .required {
			visibility: visible;
		}
	</style>
	<link rel='stylesheet' id='bootstrap-css' href='css/bootstrap.min268f.css?ver=4.5.0' type='text/css' media='all' />
	<link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='basel-style-css' href='css/style.min268f.css?ver=4.5.0' type='text/css' media='all' />
	<link rel='stylesheet' id='child-style-css' href='css/style0606.css?ver=5.2.9' type='text/css' media='all' />
	<link rel='stylesheet' id='dync-style-css' href='css/dynamic58aa.css?ver=5.0.7' type='text/css' media='all' />
	<link rel='stylesheet' id='font-awesome-css-css' href='css/font-awesome.min268f.css?ver=4.5.0' type='text/css'
		media='all' />
	<link rel='stylesheet' id='jetpack_css-css' href='css/jetpack.css' type='text/css' media='all' />
	<script type="text/template" id="tmpl-variation-template">
</script>
	<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>Rất tiếc, sản phẩm này hiện không tồn tại. Hãy chọn một phương thức kết hợp khác.</p>
</script>
	<script type='text/javascript' src='js/jquery.js'></script>
	<script type='text/javascript' src='js/device.min268f.js?ver=4.5.0'></script>
	<link rel='https://api.w.org/' href='wp-json/{{ route('trangchu') }}' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 5.0.1" />
	<meta name="generator" content="WooCommerce 3.5.2" />
	<link rel='shortlink' href='{{ route('trangchu') }}' />

	<style>
	</style>
	<link rel="shortcut icon" href="wp-content/uploads/2016/05/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="wp-content/uploads/2016/05/favicon.png">
	<style type="text/css">
		/* Shop popup */

		.basel-promo-popup {
			max-width: 900px;
		}

		.site-logo {
			width: 20%;
		}

		.site-logo img {
			max-width: 250px;
			max-height: 95px;
		}

		.right-column {
			width: 160px;
		}

		.basel-woocommerce-layered-nav .basel-scroll {
			max-height: 280px;
		}

		/* header Banner */
		.header-banner {
			height: 40px;
		}

		.header-banner-display .website-wrapper {
			margin-top: 40px;
		}

		/* Topbar height configs */

		.topbar-menu ul>li {
			line-height: 42px;
		}

		.topbar-wrapp,
		.topbar-content:before {
			height: 42px;
		}

		.sticky-header-prepared.basel-top-bar-on .header-shop,
		.sticky-header-prepared.basel-top-bar-on .header-split,
		.enable-sticky-header.basel-header-overlap.basel-top-bar-on .main-header {
			top: 42px;
		}

		/* Header height configs */

		/* Limit logo image height for according to header height */
		.site-logo img {
			max-height: 95px;
		}

		/* And for sticky header logo also */
		.act-scroll .site-logo img,
		.header-clone .site-logo img {
			max-height: 75px;
		}

		/* Set sticky headers height for cloned headers based on menu links line height */
		.header-clone .main-nav .menu>li>a {
			height: 75px;
			line-height: 75px;
		}

		/* Height for switch logos */

		.sticky-header-real:not(.global-header-menu-top) .switch-logo-enable .basel-logo {
			height: 95px;
		}

		.sticky-header-real:not(.global-header-menu-top) .act-scroll .switch-logo-enable .basel-logo {
			height: 75px;
		}

		.sticky-header-real:not(.global-header-menu-top) .act-scroll .switch-logo-enable {
			transform: translateY(-75px);
			-webkit-transform: translateY(-75px);
		}

		/* Header height for layouts that don't have line height for menu links */
		.wrapp-header {
			min-height: 95px;
		}




		/* Page headings settings for heading overlap. Calculate on the header height base */

		.basel-header-overlap .title-size-default,
		.basel-header-overlap .title-size-small,
		.basel-header-overlap .title-shop.without-title.title-size-default,
		.basel-header-overlap .title-shop.without-title.title-size-small {
			padding-top: 135px;
		}


		.basel-header-overlap .title-shop.without-title.title-size-large,
		.basel-header-overlap .title-size-large {
			padding-top: 215px;
		}

		@media (max-width: 991px) {

			/* header Banner */
			.header-banner {
				height: 40px;
			}

			.header-banner-display .website-wrapper {
				margin-top: 40px;
			}

			/* Topbar height configs */
			.topbar-menu ul>li {
				line-height: 38px;
			}

			.topbar-wrapp,
			.topbar-content:before {
				height: 38px;
			}

			.sticky-header-prepared.basel-top-bar-on .header-shop,
			.sticky-header-prepared.basel-top-bar-on .header-split,
			.enable-sticky-header.basel-header-overlap.basel-top-bar-on .main-header {
				top: 38px;
			}

			/* Set header height for mobile devices */
			.main-header .wrapp-header {
				min-height: 60px;
			}

			/* Limit logo image height for mobile according to mobile header height */
			.site-logo img {
				max-height: 60px;
			}

			/* Limit logo on sticky header. Both header real and header cloned */
			.act-scroll .site-logo img,
			.header-clone .site-logo img {
				max-height: 60px;
			}

			/* Height for switch logos */

			.main-header .switch-logo-enable .basel-logo {
				height: 60px;
			}

			.sticky-header-real:not(.global-header-menu-top) .act-scroll .switch-logo-enable .basel-logo {
				height: 60px;
			}

			.sticky-header-real:not(.global-header-menu-top) .act-scroll .switch-logo-enable {
				transform: translateY(-60px);
				-webkit-transform: translateY(-60px);
			}

			/* Page headings settings for heading overlap. Calculate on the MOBILE header height base */
			.basel-header-overlap .title-size-default,
			.basel-header-overlap .title-size-small,
			.basel-header-overlap .title-shop.without-title.title-size-default,
			.basel-header-overlap .title-shop.without-title.title-size-small {
				padding-top: 80px;
			}

			.basel-header-overlap .title-shop.without-title.title-size-large,
			.basel-header-overlap .title-size-large {
				padding-top: 120px;
			}

		}
	</style>


	<noscript>
		<style>
			.woocommerce-product-gallery {
				opacity: 1 !important;
			}
		</style>
	</noscript>
	<style type="text/css">
		/* If html does not have either class, do not show lazy loaded images. */
		html:not(.jetpack-lazy-images-js-enabled):not(.js) .jetpack-lazy-image {
			display: none;
		}
	</style>
	<script>
		document.documentElement.classList.add(
			'jetpack-lazy-images-js-enabled'
		);
	</script>
	<style type="text/css" title="dynamic-css" class="options-output">
		body,
		.basel-dark .main-page-wrapper {
			background-color: #feede6;
		}

		.page-title-default {
			background-color: #000000;
		}

		.topbar-wrapp {
			background-color: #000000;
		}

		.footer-container {
			background-color: #ffffff;
		}

		body,
		p,
		.widget_nav_mega_menu .menu>li>a,
		.mega-navigation .menu>li>a,
		.basel-navigation .menu>li.menu-item-design-full-width .sub-sub-menu li a,
		.basel-navigation .menu>li.menu-item-design-sized .sub-sub-menu li a,
		.basel-navigation .menu>li.menu-item-design-default .sub-menu li a,
		.font-default {
			font-family: Arial, Helvetica, sans-serif, Arial, Helvetica, sans-serif;
		}

		h1 a,
		h2 a,
		h3 a,
		h4 a,
		h5 a,
		h6 a,
		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		.title,
		table th,
		.wc-tabs li a,
		.masonry-filter li a,
		.woocommerce .cart-empty,
		.basel-navigation .menu>li.menu-item-design-full-width .sub-menu>li>a,
		.basel-navigation .menu>li.menu-item-design-sized .sub-menu>li>a,
		.mega-menu-list>li>a,
		fieldset legend,
		table th,
		.basel-empty-compare,
		.compare-field,
		.compare-value:before,
		.color-scheme-dark .info-box-inner h1,
		.color-scheme-dark .info-box-inner h2,
		.color-scheme-dark .info-box-inner h3,
		.color-scheme-dark .info-box-inner h4,
		.color-scheme-dark .info-box-inner h5,
		.color-scheme-dark .info-box-inner h6 {
			font-family: Arial, Helvetica, sans-serif, 'MS Sans Serif', Geneva, sans-serif;
			font-weight: normal;
			font-style: normal;
		}

		.product-title a,
		.post-slide .entry-title a,
		.category-grid-item .hover-mask h3,
		.basel-search-full-screen .basel-search-inner input[type="text"],
		.blog-post-loop .entry-title,
		.post-title-large-image .entry-title,
		.single-product-content .entry-title,
		.font-title {
			font-family: Arial, Helvetica, sans-serif, 'MS Sans Serif', Geneva, sans-serif;
			font-weight: normal;
			font-style: normal;
		}

		.title-alt,
		.subtitle,
		.font-alt,
		.basel-entry-meta {
			font-family: Arial, Helvetica, sans-serif, 'Comic Sans MS', cursive;
			font-weight: normal;
			font-style: normal;
		}

		.widgettitle,
		.widget-title {
			font-family: Arial, Helvetica, sans-serif;
			font-weight: normal;
			font-style: normal;
		}

		.main-nav .menu>li>a {
			font-family: Arial, Helvetica, sans-serif;
			font-weight: normal;
			font-style: normal;
		}

		.color-primary,
		.mobile-nav ul li.current-menu-item>a,
		.main-nav .menu>li.current-menu-item>a,
		.main-nav .menu>li.onepage-link.current-menu-item>a,
		.main-nav .menu>li>a:hover,
		.main-nav .menu>li>a:focus,
		.basel-navigation .menu>li.menu-item-design-default ul li:hover>a,
		.basel-navigation .menu>li.menu-item-design-full-width .sub-menu li a:hover,
		.basel-navigation .menu>li.menu-item-design-sized .sub-menu li a:hover,
		.basel-product-categories.responsive-cateogires li.current-cat>a,
		.basel-product-categories.responsive-cateogires li.current-cat-parent>a,
		.basel-product-categories.responsive-cateogires li.current-cat-ancestor>a,
		.basel-my-account-links a:hover:before,
		.basel-my-account-links a:focus:before,
		.mega-menu-list>li>a:hover,
		.mega-menu-list .sub-sub-menu li a:hover,
		a[href^=tel],
		.topbar-menu ul>li>.sub-menu-dropdown li>a:hover,
		.btn.btn-color-primary.btn-style-bordered,
		.button.btn-color-primary.btn-style-bordered,
		button.btn-color-primary.btn-style-bordered,
		.added_to_cart.btn-color-primary.btn-style-bordered,
		input[type=submit].btn-color-primary.btn-style-bordered,
		a.login-to-prices-msg,
		a.login-to-prices-msg:hover,
		.basel-dark .single-product-content .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a:before,
		.basel-dark .single-product-content .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a:before,
		.basel-dark .read-more-section .btn-read-more,
		.basel-dark .products-footer .basel-blog-load-more,
		.basel-dark .products-footer .basel-products-load-more,
		.basel-dark .products-footer .basel-portfolio-load-more,
		.basel-dark .blog-footer .basel-blog-load-more,
		.basel-dark .blog-footer .basel-products-load-more,
		.basel-dark .blog-footer .basel-portfolio-load-more,
		.basel-dark .portfolio-footer .basel-blog-load-more,
		.basel-dark .portfolio-footer .basel-products-load-more,
		.basel-dark .portfolio-footer .basel-portfolio-load-more,
		.basel-dark .color-primary,
		.basel-hover-link .swap-elements .btn-add a,
		.basel-hover-link .swap-elements .btn-add a:hover,
		.basel-hover-link .swap-elements .btn-add a:focus,
		.blog-post-loop .entry-title a:hover,
		.blog-post-loop.sticky .entry-title:before,
		.post-slide .entry-title a:hover,
		.comments-area .reply a,
		.single-post-navigation a:hover,
		blockquote footer:before,
		blockquote cite,
		.format-quote .entry-content blockquote cite,
		.format-quote .entry-content blockquote cite a,
		.basel-entry-meta .meta-author a,
		.search-no-results.woocommerce .site-content:before,
		.search-no-results .not-found .entry-header:before,
		.login-form-footer .lost_password:hover,
		.login-form-footer .lost_password:focus,
		.error404 .page-title,
		.menu-label-new:after,
		.widget_shopping_cart .product_list_widget li .quantity .amount,
		.product_list_widget li ins .amount,
		.price ins>.amount,
		.price ins,
		.single-product .price,
		.single-product .price .amount,
		.popup-quick-view .price,
		.popup-quick-view .price .amount,
		.basel-products-nav .product-short .price,
		.basel-products-nav .product-short .price .amount,
		.star-rating span:before,
		.single-product-content .comment-form .stars span a:hover,
		.single-product-content .comment-form .stars span a.active,
		.tabs-layout-accordion .basel-tab-wrapper .basel-accordion-title:hover,
		.tabs-layout-accordion .basel-tab-wrapper .basel-accordion-title.active,
		.single-product-content .woocommerce-product-details__short-description ul>li:before,
		.single-product-content #tab-description ul>li:before,
		.blog-post-loop .entry-content ul>li:before,
		.comments-area .comment-list li ul>li:before,
		.brands-list .brand-item a:hover,
		.sidebar-widget li a:hover,
		.filter-widget li a:hover,
		.sidebar-widget li>ul li a:hover,
		.filter-widget li>ul li a:hover,
		.basel-price-filter ul li a:hover .amount,
		.basel-hover-effect-4 .swap-elements>a,
		.basel-hover-effect-4 .swap-elements>a:hover,
		.product-grid-item .basel-product-cats a:hover,
		.product-grid-item .basel-product-brands-links a:hover,
		.wishlist_table tr td.product-price ins .amount,
		.basel-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse>a,
		.basel-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse>a,
		.basel-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse>a:hover,
		.basel-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse>a:hover,
		.basel-buttons .product-compare-button>a.added:before,
		.single-product-content .entry-summary .yith-wcwl-add-to-wishlist a:hover,
		.single-product-content .container .entry-summary .yith-wcwl-add-to-wishlist a:hover:before,
		.single-product-content .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a:before,
		.single-product-content .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a:before,
		.single-product-content .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button.feid-in>a:before,
		.basel-sticky-btn .basel-sticky-btn-wishlist.exists,
		.basel-sticky-btn .basel-sticky-btn-wishlist:hover,
		.vendors-list ul li a:hover,
		.product-list-item .product-list-buttons .yith-wcwl-add-to-wishlist a:hover,
		.product-list-item .product-list-buttons .yith-wcwl-add-to-wishlist a:focus,
		.product-list-item .product-list-buttons .product-compare-button a:hover,
		.product-list-item .product-list-buttons .product-compare-button a:focus,
		.product-list-item .product-list-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse>a:before,
		.product-list-item .product-list-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse>a:before,
		.product-list-item .product-list-buttons .product-compare-button>a.added:before,
		.single-product-content .entry-summary .compare-btn-wrapper a:hover,
		.single-product-content .entry-summary .compare-btn-wrapper a:hover:before,
		.single-product-content .entry-summary .compare-btn-wrapper a.added:before,
		.single-product-content .entry-summary .basel-sizeguide-btn:hover,
		.single-product-content .entry-summary .basel-sizeguide-btn:hover:before,
		.blog-post-loop .entry-content ul li:before,
		.basel-menu-price .menu-price-price,
		.basel-menu-price.cursor-pointer:hover .menu-price-title,
		.comments-area #cancel-comment-reply-link:hover,
		.comments-area .comment-body .comment-edit-link:hover,
		.popup-quick-view .entry-summary .entry-title a:hover,
		.wpb_text_column ul:not(.social-icons)>li:before,
		.widget_product_categories .basel-cats-toggle:hover,
		.widget_product_categories .toggle-active,
		.widget_product_categories .current-cat-parent>a,
		.widget_product_categories .current-cat>a,
		.woocommerce-checkout-review-order-table tfoot .order-total td .amount,
		.widget_shopping_cart .product_list_widget li .remove:hover,
		.basel-active-filters .widget_layered_nav_filters ul li a .amount,
		.title-wrapper.basel-title-color-primary .title-subtitle,
		.widget_shopping_cart .widget_shopping_cart_content>.total .amount,
		.color-scheme-light .vc_tta-tabs.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tab.vc_active>a,
		.wpb-js-composer .vc_tta.vc_general.vc_tta-style-classic .vc_tta-tab.vc_active>a {
			color: #eb278d;
		}

		.right-column .wishlist-info-widget>a>span,
		.basel-cart-design-2>a .basel-cart-number,
		.basel-cart-design-3>a .basel-cart-number,
		.btn.btn-color-primary,
		.button.btn-color-primary,
		button.btn-color-primary,
		.added_to_cart.btn-color-primary,
		input[type=submit].btn-color-primary,
		.btn.btn-color-primary:hover,
		.btn.btn-color-primary:focus,
		.button.btn-color-primary:hover,
		.button.btn-color-primary:focus,
		button.btn-color-primary:hover,
		button.btn-color-primary:focus,
		.added_to_cart.btn-color-primary:hover,
		.added_to_cart.btn-color-primary:focus,
		input[type=submit].btn-color-primary:hover,
		input[type=submit].btn-color-primary:focus,
		.btn.btn-color-primary.btn-style-bordered:hover,
		.btn.btn-color-primary.btn-style-bordered:focus,
		.button.btn-color-primary.btn-style-bordered:hover,
		.button.btn-color-primary.btn-style-bordered:focus,
		button.btn-color-primary.btn-style-bordered:hover,
		button.btn-color-primary.btn-style-bordered:focus,
		.added_to_cart.btn-color-primary.btn-style-bordered:hover,
		.added_to_cart.btn-color-primary.btn-style-bordered:focus,
		input[type=submit].btn-color-primary.btn-style-bordered:hover,
		input[type=submit].btn-color-primary.btn-style-bordered:focus,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout:hover,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout:focus,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button:hover,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button:focus,
		.no-results .searchform #searchsubmit,
		.no-results .searchform #searchsubmit:hover,
		.no-results .searchform #searchsubmit:focus,
		.comments-area .comment-respond input[type=submit],
		.comments-area .comment-respond input[type=submit]:hover,
		.comments-area .comment-respond input[type=submit]:focus,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button:hover,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button:focus,
		.woocommerce .checkout_coupon .button,
		.woocommerce .checkout_coupon .button:hover,
		.woocommerce .checkout_coupon .button:focus,
		.woocommerce .place-order button,
		.woocommerce .place-order button:hover,
		.woocommerce .place-order button:focus,
		.woocommerce-order-pay #order_review .button,
		.woocommerce-order-pay #order_review .button:hover,
		.woocommerce-order-pay #order_review .button:focus,
		.woocommerce input[name=track],
		.woocommerce input[name=track]:hover,
		.woocommerce input[name=track]:focus,
		.woocommerce input[name=save_account_details],
		.woocommerce input[name=save_address],
		.woocommerce-page input[name=save_account_details],
		.woocommerce-page input[name=save_address],
		.woocommerce input[name=save_account_details]:hover,
		.woocommerce input[name=save_account_details]:focus,
		.woocommerce input[name=save_address]:hover,
		.woocommerce input[name=save_address]:focus,
		.woocommerce-page input[name=save_account_details]:hover,
		.woocommerce-page input[name=save_account_details]:focus,
		.woocommerce-page input[name=save_address]:hover,
		.woocommerce-page input[name=save_address]:focus,
		.search-no-results .not-found .entry-content .searchform #searchsubmit,
		.search-no-results .not-found .entry-content .searchform #searchsubmit:hover,
		.search-no-results .not-found .entry-content .searchform #searchsubmit:focus,
		.error404 .page-content>.searchform #searchsubmit,
		.error404 .page-content>.searchform #searchsubmit:hover,
		.error404 .page-content>.searchform #searchsubmit:focus,
		.return-to-shop .button,
		.return-to-shop .button:hover,
		.return-to-shop .button:focus,
		.basel-hover-excerpt .btn-add a,
		.basel-hover-excerpt .btn-add a:hover,
		.basel-hover-excerpt .btn-add a:focus,
		.basel-hover-standard .btn-add>a,
		.basel-hover-standard .btn-add>a:hover,
		.basel-hover-standard .btn-add>a:focus,
		.basel-price-table .basel-plan-footer>a,
		.basel-price-table .basel-plan-footer>a:hover,
		.basel-price-table .basel-plan-footer>a:focus,
		.basel-pf-btn button,
		.basel-pf-btn button:hover,
		.basel-pf-btn button:focus,
		.basel-info-box.box-style-border .info-btn-wrapper a,
		.basel-info-box.box-style-border .info-btn-wrapper a:hover,
		.basel-info-box.box-style-border .info-btn-wrapper a:focus,
		.basel-info-box2.box-style-border .info-btn-wrapper a,
		.basel-info-box2.box-style-border .info-btn-wrapper a:hover,
		.basel-info-box2.box-style-border .info-btn-wrapper a:focus,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button:hover,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button:focus,
		.product-list-item .product-list-buttons>a,
		.product-list-item .product-list-buttons>a:hover,
		.product-list-item .product-list-buttons>a:focus,
		.wpb_video_wrapper .button-play,
		.basel-navigation .menu>li.callto-btn>a,
		.basel-navigation .menu>li.callto-btn>a:hover,
		.basel-navigation .menu>li.callto-btn>a:focus,
		.basel-dark .products-footer .basel-blog-load-more:hover,
		.basel-dark .products-footer .basel-blog-load-more:focus,
		.basel-dark .products-footer .basel-products-load-more:hover,
		.basel-dark .products-footer .basel-products-load-more:focus,
		.basel-dark .products-footer .basel-portfolio-load-more:hover,
		.basel-dark .products-footer .basel-portfolio-load-more:focus,
		.basel-dark .blog-footer .basel-blog-load-more:hover,
		.basel-dark .blog-footer .basel-blog-load-more:focus,
		.basel-dark .blog-footer .basel-products-load-more:hover,
		.basel-dark .blog-footer .basel-products-load-more:focus,
		.basel-dark .blog-footer .basel-portfolio-load-more:hover,
		.basel-dark .blog-footer .basel-portfolio-load-more:focus,
		.basel-dark .portfolio-footer .basel-blog-load-more:hover,
		.basel-dark .portfolio-footer .basel-blog-load-more:focus,
		.basel-dark .portfolio-footer .basel-products-load-more:hover,
		.basel-dark .portfolio-footer .basel-products-load-more:focus,
		.basel-dark .portfolio-footer .basel-portfolio-load-more:hover,
		.basel-dark .portfolio-footer .basel-portfolio-load-more:focus,
		.basel-dark .feedback-form .wpcf7-submit,
		.basel-dark .mc4wp-form input[type=submit],
		.basel-dark .single_add_to_cart_button,
		.basel-dark .basel-compare-col .add_to_cart_button,
		.basel-dark .basel-compare-col .added_to_cart,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit],
		.basel-dark .basel-registration-page .basel-switch-to-register,
		.basel-dark .register .button,
		.basel-dark .login .button,
		.basel-dark .lost_reset_password .button,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button,
		.basel-dark .woocommerce .cart-actions .coupon .button,
		.basel-dark .feedback-form .wpcf7-submit:hover,
		.basel-dark .mc4wp-form input[type=submit]:hover,
		.basel-dark .single_add_to_cart_button:hover,
		.basel-dark .basel-compare-col .add_to_cart_button:hover,
		.basel-dark .basel-compare-col .added_to_cart:hover,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart:hover,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit]:hover,
		.basel-dark .basel-registration-page .basel-switch-to-register:hover,
		.basel-dark .register .button:hover,
		.basel-dark .login .button:hover,
		.basel-dark .lost_reset_password .button:hover,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button:hover,
		.basel-dark .woocommerce .cart-actions .coupon .button:hover,
		.basel-ext-primarybtn-dark:focus,
		.basel-dark .feedback-form .wpcf7-submit:focus,
		.basel-dark .mc4wp-form input[type=submit]:focus,
		.basel-dark .single_add_to_cart_button:focus,
		.basel-dark .basel-compare-col .add_to_cart_button:focus,
		.basel-dark .basel-compare-col .added_to_cart:focus,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart:focus,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit]:focus,
		.basel-dark .basel-registration-page .basel-switch-to-register:focus,
		.basel-dark .register .button:focus,
		.basel-dark .login .button:focus,
		.basel-dark .lost_reset_password .button:focus,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button:focus,
		.basel-dark .woocommerce .cart-actions .coupon .button:focus,
		.basel-stock-progress-bar .progress-bar,
		.widget_price_filter .ui-slider .ui-slider-handle:after,
		.widget_price_filter .ui-slider .ui-slider-range,
		.widget_tag_cloud .tagcloud a:hover,
		.widget_product_tag_cloud .tagcloud a:hover,
		div.bbp-submit-wrapper button,
		div.bbp-submit-wrapper button:hover,
		div.bbp-submit-wrapper button:focus,
		#bbpress-forums .bbp-search-form #bbp_search_submit,
		#bbpress-forums .bbp-search-form #bbp_search_submit:hover,
		#bbpress-forums .bbp-search-form #bbp_search_submit:focus,
		.select2-container--default .select2-results__option--highlighted[aria-selected],
		.product-video-button a:hover:before,
		.product-360-button a:hover:before,
		.mobile-nav ul li .up-icon,
		.scrollToTop:hover,
		.scrollToTop:focus,
		.basel-sticky-filter-btn:hover,
		.basel-sticky-filter-btn:focus,
		.categories-opened li a:active,
		.basel-price-table .basel-plan-price,
		.header-categories .secondary-header .mega-navigation,
		.widget_nav_mega_menu,
		.meta-post-categories,
		.slider-title:before,
		.title-wrapper.basel-title-style-simple .title:after,
		.menu-label-new,
		.product-label.onsale,
		.color-scheme-light .vc_tta-tabs.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tab.vc_active>a span:after,
		.wpb-js-composer .vc_tta.vc_general.vc_tta-style-classic .vc_tta-tab.vc_active>a span:after,
		.portfolio-with-bg-alt .portfolio-entry:hover .entry-header>.portfolio-info {
			background-color: #eb278d;
		}

		.btn.btn-color-primary,
		.button.btn-color-primary,
		button.btn-color-primary,
		.added_to_cart.btn-color-primary,
		input[type=submit].btn-color-primary,
		.btn.btn-color-primary:hover,
		.btn.btn-color-primary:focus,
		.button.btn-color-primary:hover,
		.button.btn-color-primary:focus,
		button.btn-color-primary:hover,
		button.btn-color-primary:focus,
		.added_to_cart.btn-color-primary:hover,
		.added_to_cart.btn-color-primary:focus,
		input[type=submit].btn-color-primary:hover,
		input[type=submit].btn-color-primary:focus,
		.btn.btn-color-primary.btn-style-bordered:hover,
		.btn.btn-color-primary.btn-style-bordered:focus,
		.button.btn-color-primary.btn-style-bordered:hover,
		.button.btn-color-primary.btn-style-bordered:focus,
		button.btn-color-primary.btn-style-bordered:hover,
		button.btn-color-primary.btn-style-bordered:focus,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout:hover,
		.widget_shopping_cart .widget_shopping_cart_content .buttons .checkout:focus,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button:hover,
		.basel-search-dropdown .basel-search-wrapper .basel-search-inner form button:focus,
		.comments-area .comment-respond input[type=submit],
		.comments-area .comment-respond input[type=submit]:hover,
		.comments-area .comment-respond input[type=submit]:focus,
		.sidebar-container .mc4wp-form input[type=submit],
		.sidebar-container .mc4wp-form input[type=submit]:hover,
		.sidebar-container .mc4wp-form input[type=submit]:focus,
		.footer-container .mc4wp-form input[type=submit],
		.footer-container .mc4wp-form input[type=submit]:hover,
		.footer-container .mc4wp-form input[type=submit]:focus,
		.filters-area .mc4wp-form input[type=submit],
		.filters-area .mc4wp-form input[type=submit]:hover,
		.filters-area .mc4wp-form input[type=submit]:focus,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button:hover,
		.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout>a.button:focus,
		.woocommerce .checkout_coupon .button,
		.woocommerce .checkout_coupon .button:hover,
		.woocommerce .checkout_coupon .button:focus,
		.woocommerce .place-order button,
		.woocommerce .place-order button:hover,
		.woocommerce .place-order button:focus,
		.woocommerce-order-pay #order_review .button,
		.woocommerce-order-pay #order_review .button:hover,
		.woocommerce-order-pay #order_review .button:focus,
		.woocommerce input[name=track],
		.woocommerce input[name=track]:hover,
		.woocommerce input[name=track]:focus,
		.woocommerce input[name=save_account_details],
		.woocommerce input[name=save_address],
		.woocommerce-page input[name=save_account_details],
		.woocommerce-page input[name=save_address],
		.woocommerce input[name=save_account_details]:hover,
		.woocommerce input[name=save_account_details]:focus,
		.woocommerce input[name=save_address]:hover,
		.woocommerce input[name=save_address]:focus,
		.woocommerce-page input[name=save_account_details]:hover,
		.woocommerce-page input[name=save_account_details]:focus,
		.woocommerce-page input[name=save_address]:hover,
		.woocommerce-page input[name=save_address]:focus,
		.search-no-results .not-found .entry-content .searchform #searchsubmit,
		.search-no-results .not-found .entry-content .searchform #searchsubmit:hover,
		.search-no-results .not-found .entry-content .searchform #searchsubmit:focus,
		.error404 .page-content>.searchform #searchsubmit,
		.error404 .page-content>.searchform #searchsubmit:hover,
		.error404 .page-content>.searchform #searchsubmit:focus,
		.no-results .searchform #searchsubmit,
		.no-results .searchform #searchsubmit:hover,
		.no-results .searchform #searchsubmit:focus,
		.return-to-shop .button,
		.return-to-shop .button:hover,
		.return-to-shop .button:focus,
		.basel-hover-excerpt .btn-add a,
		.basel-hover-excerpt .btn-add a:hover,
		.basel-hover-excerpt .btn-add a:focus,
		.basel-hover-standard .btn-add>a,
		.basel-hover-standard .btn-add>a:hover,
		.basel-hover-standard .btn-add>a:focus,
		.basel-price-table .basel-plan-footer>a,
		.basel-price-table .basel-plan-footer>a:hover,
		.basel-price-table .basel-plan-footer>a:focus,
		.basel-pf-btn button,
		.basel-pf-btn button:hover,
		.basel-pf-btn button:focus,
		.basel-info-box.box-style-border .info-btn-wrapper a,
		.basel-info-box.box-style-border .info-btn-wrapper a:hover,
		.basel-info-box.box-style-border .info-btn-wrapper a:focus,
		.basel-info-box2.box-style-border .info-btn-wrapper a,
		.basel-info-box2.box-style-border .info-btn-wrapper a:hover,
		.basel-info-box2.box-style-border .info-btn-wrapper a:focus,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button:hover,
		.basel-hover-quick .woocommerce-variation-add-to-cart .button:focus,
		.product-list-item .product-list-buttons>a,
		.product-list-item .product-list-buttons>a:hover,
		.product-list-item .product-list-buttons>a:focus,
		.wpb_video_wrapper .button-play,
		.woocommerce-store-notice__dismiss-link:hover,
		.woocommerce-store-notice__dismiss-link:focus,
		.basel-compare-table .compare-loader:after,
		.basel-dark .read-more-section .btn-read-more,
		.basel-dark .products-footer .basel-blog-load-more,
		.basel-dark .products-footer .basel-products-load-more,
		.basel-dark .products-footer .basel-portfolio-load-more,
		.basel-dark .blog-footer .basel-blog-load-more,
		.basel-dark .blog-footer .basel-products-load-more,
		.basel-dark .blog-footer .basel-portfolio-load-more,
		.basel-dark .portfolio-footer .basel-blog-load-more,
		.basel-dark .portfolio-footer .basel-products-load-more,
		.basel-dark .portfolio-footer .basel-portfolio-load-more,
		.basel-dark .products-footer .basel-blog-load-more:hover,
		.basel-dark .products-footer .basel-blog-load-more:focus,
		.basel-dark .products-footer .basel-products-load-more:hover,
		.basel-dark .products-footer .basel-products-load-more:focus,
		.basel-dark .products-footer .basel-portfolio-load-more:hover,
		.basel-dark .products-footer .basel-portfolio-load-more:focus,
		.basel-dark .blog-footer .basel-blog-load-more:hover,
		.basel-dark .blog-footer .basel-blog-load-more:focus,
		.basel-dark .blog-footer .basel-products-load-more:hover,
		.basel-dark .blog-footer .basel-products-load-more:focus,
		.basel-dark .blog-footer .basel-portfolio-load-more:hover,
		.basel-dark .blog-footer .basel-portfolio-load-more:focus,
		.basel-dark .portfolio-footer .basel-blog-load-more:hover,
		.basel-dark .portfolio-footer .basel-blog-load-more:focus,
		.basel-dark .portfolio-footer .basel-products-load-more:hover,
		.basel-dark .portfolio-footer .basel-products-load-more:focus,
		.basel-dark .portfolio-footer .basel-portfolio-load-more:hover,
		.basel-dark .portfolio-footer .basel-portfolio-load-more:focus,
		.basel-dark .products-footer .basel-blog-load-more:after,
		.basel-dark .products-footer .basel-products-load-more:after,
		.basel-dark .products-footer .basel-portfolio-load-more:after,
		.basel-dark .blog-footer .basel-blog-load-more:after,
		.basel-dark .blog-footer .basel-products-load-more:after,
		.basel-dark .blog-footer .basel-portfolio-load-more:after,
		.basel-dark .portfolio-footer .basel-blog-load-more:after,
		.basel-dark .portfolio-footer .basel-products-load-more:after,
		.basel-dark .portfolio-footer .basel-portfolio-load-more:after,
		.basel-dark .feedback-form .wpcf7-submit,
		.basel-dark .mc4wp-form input[type=submit],
		.basel-dark .single_add_to_cart_button,
		.basel-dark .basel-compare-col .add_to_cart_button,
		.basel-dark .basel-compare-col .added_to_cart,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit],
		.basel-dark .basel-registration-page .basel-switch-to-register,
		.basel-dark .register .button,
		.basel-dark .login .button,
		.basel-dark .lost_reset_password .button,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button,
		.basel-dark .woocommerce .cart-actions .coupon .button,
		.basel-dark .feedback-form .wpcf7-submit:hover,
		.basel-dark .mc4wp-form input[type=submit]:hover,
		.basel-dark .single_add_to_cart_button:hover,
		.basel-dark .basel-compare-col .add_to_cart_button:hover,
		.basel-dark .basel-compare-col .added_to_cart:hover,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart:hover,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit]:hover,
		.basel-dark .basel-registration-page .basel-switch-to-register:hover,
		.basel-dark .register .button:hover,
		.basel-dark .login .button:hover,
		.basel-dark .lost_reset_password .button:hover,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button:hover,
		.basel-dark .woocommerce .cart-actions .coupon .button:hover,
		.basel-ext-primarybtn-dark:focus,
		.basel-dark .feedback-form .wpcf7-submit:focus,
		.basel-dark .mc4wp-form input[type=submit]:focus,
		.basel-dark .single_add_to_cart_button:focus,
		.basel-dark .basel-compare-col .add_to_cart_button:focus,
		.basel-dark .basel-compare-col .added_to_cart:focus,
		.basel-dark .basel-sticky-btn .basel-sticky-add-to-cart:focus,
		.basel-dark .single-product-content .comment-form .form-submit input[type=submit]:focus,
		.basel-dark .basel-registration-page .basel-switch-to-register:focus,
		.basel-dark .register .button:focus,
		.basel-dark .login .button:focus,
		.basel-dark .lost_reset_password .button:focus,
		.basel-dark .wishlist_table tr td.product-add-to-cart>.add_to_cart.button:focus,
		.basel-dark .woocommerce .cart-actions .coupon .button:focus,
		.cookies-buttons .cookies-accept-btn:hover,
		.cookies-buttons .cookies-accept-btn:focus,
		.blockOverlay:after,
		.basel-price-table:hover,
		.title-shop .nav-shop ul li a:after,
		.widget_tag_cloud .tagcloud a:hover,
		.widget_product_tag_cloud .tagcloud a:hover,
		div.bbp-submit-wrapper button,
		div.bbp-submit-wrapper button:hover,
		div.bbp-submit-wrapper button:focus,
		#bbpress-forums .bbp-search-form #bbp_search_submit,
		#bbpress-forums .bbp-search-form #bbp_search_submit:hover,
		#bbpress-forums .bbp-search-form #bbp_search_submit:focus,
		.basel-hover-link .swap-elements .btn-add a,
		.basel-hover-link .swap-elements .btn-add a:hover,
		.basel-hover-link .swap-elements .btn-add a:focus,
		.basel-hover-link .swap-elements .btn-add a.loading:after,
		.scrollToTop:hover,
		.scrollToTop:focus,
		.basel-sticky-filter-btn:hover,
		.basel-sticky-filter-btn:focus,
		blockquote {
			border-color: #eb278d;
		}

		.with-animation .info-box-icon svg path,
		.single-product-content .entry-summary .basel-sizeguide-btn:hover svg {
			stroke: #eb278d;
		}

		.button,
		button,
		input[type=submit],
		.yith-woocompare-widget a.button.compare,
		.basel-dark .basel-registration-page .basel-switch-to-register,
		.basel-dark .login .button,
		.basel-dark .register .button,
		.basel-dark .widget_shopping_cart .buttons a,
		.basel-dark .yith-woocompare-widget a.button.compare,
		.basel-dark .widget_price_filter .price_slider_amount .button,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"] {
			background-color: #ECECEC;
		}

		.button,
		button,
		input[type=submit],
		.yith-woocompare-widget a.button.compare,
		.basel-dark .basel-registration-page .basel-switch-to-register,
		.basel-dark .login .button,
		.basel-dark .register .button,
		.basel-dark .widget_shopping_cart .buttons a,
		.basel-dark .yith-woocompare-widget a.button.compare,
		.basel-dark .widget_price_filter .price_slider_amount .button,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"] {
			border-color: #ECECEC;
		}

		.button:hover,
		.button:focus,
		button:hover,
		button:focus,
		input[type=submit]:hover,
		input[type=submit]:focus,
		.yith-woocompare-widget a.button.compare:hover,
		.yith-woocompare-widget a.button.compare:focus,
		.basel-dark .basel-registration-page .basel-switch-to-register:hover,
		.basel-dark .basel-registration-page .basel-switch-to-register:focus,
		.basel-dark .login .button:hover,
		.basel-dark .login .button:focus,
		.basel-dark .register .button:hover,
		.basel-dark .register .button:focus,
		.basel-dark .widget_shopping_cart .buttons a:hover,
		.basel-dark .widget_shopping_cart .buttons a:focus,
		.basel-dark .yith-woocompare-widget a.button.compare:hover,
		.basel-dark .yith-woocompare-widget a.button.compare:focus,
		.basel-dark .widget_price_filter .price_slider_amount .button:hover,
		.basel-dark .widget_price_filter .price_slider_amount .button:focus,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"]:hover,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"]:focus {
			background-color: #3E3E3E;
		}

		.button:hover,
		.button:focus,
		button:hover,
		button:focus,
		input[type=submit]:hover,
		input[type=submit]:focus,
		.yith-woocompare-widget a.button.compare:hover,
		.yith-woocompare-widget a.button.compare:focus,
		.basel-dark .basel-registration-page .basel-switch-to-register:hover,
		.basel-dark .basel-registration-page .basel-switch-to-register:focus,
		.basel-dark .login .button:hover,
		.basel-dark .login .button:focus,
		.basel-dark .register .button:hover,
		.basel-dark .register .button:focus,
		.basel-dark .widget_shopping_cart .buttons a:hover,
		.basel-dark .widget_shopping_cart .buttons a:focus,
		.basel-dark .yith-woocompare-widget a.button.compare:hover,
		.basel-dark .yith-woocompare-widget a.button.compare:focus,
		.basel-dark .widget_price_filter .price_slider_amount .button:hover,
		.basel-dark .widget_price_filter .price_slider_amount .button:focus,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"]:hover,
		.basel-dark .woocommerce .cart-actions input[name="update_cart"]:focus {
			border-color: #3E3E3E;
		}

		.single_add_to_cart_button,
		.basel-sticky-btn .basel-sticky-add-to-cart,
		.woocommerce .cart-actions .coupon .button,
		.added_to_cart.btn-color-black,
		input[type=submit].btn-color-black,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button,
		.basel-hover-quick .quick-shop-btn>a,
		table.compare-list tr.add-to-cart td a,
		.basel-compare-col .add_to_cart_button,
		.basel-compare-col .added_to_cart {
			background-color: #000000;
		}

		.single_add_to_cart_button,
		.basel-sticky-btn .basel-sticky-add-to-cart,
		.woocommerce .cart-actions .coupon .button,
		.added_to_cart.btn-color-black,
		input[type=submit].btn-color-black,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button,
		.basel-hover-quick .quick-shop-btn>a,
		table.compare-list tr.add-to-cart td a,
		.basel-compare-col .add_to_cart_button,
		.basel-compare-col .added_to_cart {
			border-color: #000000;
		}

		.basel-hover-alt .btn-add>a {
			color: #000000;
		}

		.single_add_to_cart_button:hover,
		.single_add_to_cart_button:focus,
		.basel-sticky-btn .basel-sticky-add-to-cart:hover,
		.basel-sticky-btn .basel-sticky-add-to-cart:focus,
		.woocommerce .cart-actions .coupon .button:hover,
		.woocommerce .cart-actions .coupon .button:focus,
		.added_to_cart.btn-color-black:hover,
		.added_to_cart.btn-color-black:focus,
		input[type=submit].btn-color-black:hover,
		input[type=submit].btn-color-black:focus,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button:hover,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button:focus,
		.basel-hover-quick .quick-shop-btn>a:hover,
		.basel-hover-quick .quick-shop-btn>a:focus,
		table.compare-list tr.add-to-cart td a:hover,
		table.compare-list tr.add-to-cart td a:focus,
		.basel-compare-col .add_to_cart_button:hover,
		.basel-compare-col .add_to_cart_button:focus,
		.basel-compare-col .added_to_cart:hover,
		.basel-compare-col .added_to_cart:focus {
			background-color: #333333;
		}

		.single_add_to_cart_button:hover,
		.single_add_to_cart_button:focus,
		.basel-sticky-btn .basel-sticky-add-to-cart:hover,
		.basel-sticky-btn .basel-sticky-add-to-cart:focus,
		.woocommerce .cart-actions .coupon .button:hover,
		.woocommerce .cart-actions .coupon .button:focus,
		.added_to_cart.btn-color-black:hover,
		.added_to_cart.btn-color-black:focus,
		input[type=submit].btn-color-black:hover,
		input[type=submit].btn-color-black:focus,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button:hover,
		.wishlist_table tr td.product-add-to-cart>.add_to_cart.button:focus,
		.basel-hover-quick .quick-shop-btn>a:hover,
		.basel-hover-quick .quick-shop-btn>a:focus,
		table.compare-list tr.add-to-cart td a:hover,
		table.compare-list tr.add-to-cart td a:focus,
		.basel-compare-col .add_to_cart_button:hover,
		.basel-compare-col .add_to_cart_button:focus,
		.basel-compare-col .added_to_cart:hover,
		.basel-compare-col .added_to_cart:focus {
			border-color: #333333;
		}

		.basel-hover-alt .btn-add>a:hover,
		.basel-hover-alt .btn-add>a:focus {
			color: #333333;
		}

		.basel-promo-popup {
			background-repeat: no-repeat;
			background-size: contain;
			background-position: left center;
			background-image: url('https://placehold.it/760x800');
		}
	</style>
	<script async src='js/s-201918.js'></script>

</head>

<body data-rsssl=1
	class="home page-template page-template-templates-home page-template-templates-home-php page page-id-151 woocommerce-no-js wrapper-full-width global-cart-design-1 global-search-disable global-header-base mobile-nav-from-left basel-light catalog-mode-off categories-accordion-on global-wishlist-enable basel-top-bar-off basel-ajax-shop-off basel-ajax-search-on enable-sticky-header sticky-header-clone offcanvas-sidebar-mobile offcanvas-sidebar-tablet">
	<div class="login-form-side">
		<div class="widget-heading">
			<h3 class="widget-title">Đăng nhập</h3>
			<a href="#" class="widget-close">đóng</a>
		</div>

		<div class="login-form">
			<form method="post" class="login woocommerce-form woocommerce-form-login " action="{{ route('trangchu') }}">



				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-username">
					<label for="username">Tên đang nhập hoặc email&nbsp;<span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username"
						autocomplete="username" value="" />
				</p>
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-password">
					<label for="password">Mật khẩu&nbsp;<span class="required">*</span></label>
					<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password"
						id="password" autocomplete="current-password" />
				</p>


				<p class="form-row">
					<input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="89d89fb0a2" /><input
						type="hidden" name="_wp_http_referer" value="/" /> <button type="submit" class="button woocommerce-Button"
						name="login" value="Đăng nhập">Đăng nhập</button>
				</p>

				<div class="login-form-footer">
					<a href="{{ route('trangchu') }}" class="woocommerce-LostPassword lost_password">Quên mật khẩu</a>
					<label for="rememberme" class="remember-me-label inline">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox"
							value="forever" /> <span>Nhớ mật khẩu
						</span>
					</label>
				</div>

				<span class="social-login-title">Hoặc đăng nhập bằng
				</span>
				<div class="basel-social-login">
					<div class="social-login-btn">
						<a href="{{ route('trangchu') }}"
							class="btn login-fb-link">Facebook</a>
					</div>
					<div class="social-login-btn">
						<a href="{{ route('trangchu') }}"
							class="btn login-goo-link">Google</a>
					</div>
				</div>


			</form>

		</div>

		<div class="register-question">
			<span class="create-account-text">Bạn chưa có tài khoản ? </span>
			<a class="btn btn-style-link" href="tai-khoan.html">Tạo tài khoản</a>
		</div>
	</div>
	@include('pages/header')
	

		<div class="clear"></div>

		<div class="main-page-wrapper">
				@yield('content')
		</div><!-- .main-page-wrapper -->

		@include('pages/footer')


	</div> <!-- end wrapper -->

	<div class="basel-close-side"></div>

	<section class="navi-fixed visible-mobile">
		<ul class="mini-navi">
			<li class="visible-mobile">
				<a href="{{ route('trangchu') }}" target="_parent" data-attr="facebook_messenger">
					<i class="fa fa-facebook"
						style=" font-size: 25px; max-width: 25px; margin-bottom: 5px; vertical-align: middle; "></i>
					<strong class="mini-navi-title">Trang chủ</strong>
				</a>
			</li>
			<li class="visible-mobile">
				<a href="tel:1900636737"
					onclick="ga('send', 'event', { eventCategory: 'Gọi điện thoại 1900636737', eventAction: 'Call', eventLabel: 'Call'});">
					<img class="mini-navi-icon" src="wp-content/themes/nuty-suntorydesign-child/images/phone.png">
					<strong class="mini-navi-title">1800.xxxx.xxxx</strong>
				</a>
			</li>
			<li class="visible-mobile">
				<a href="sms:+84886838939"
					onclick="ga('send', 'event', { eventCategory: 'Nhắn tin 84886838939', eventAction: 'Message', eventLabel: 'Mobile Message'});">
					<img onclick="vgc_sh_chat_contact();" class="mini-navi-icon"
						src="wp-content/themes/nuty-suntorydesign-child/images/comment.png">
					<strong onclick="vgc_sh_chat_contact();" class="mini-navi-title">Sản phẩm</strong>
				</a>
			</li>
			<li>
				<a href="{{ route('trangchu') }}"
					onclick="ga('send', 'event', { eventCategory: 'Thanh toán', eventAction: 'Checkout', eventLabel: 'Mobile Checkout'});">
					<span class="mini-cart-img-wrap">
						<img class="mini-navi-icon" src="wp-content/themes/nuty-suntorydesign-child/images/cart.png">

					</span>
					<strong class="mini-navi-title">Đăng ký</strong>
				</a>
			</li>
			<li>
				<a href="{{ route('trangchu') }}"
					onclick="ga('send', 'event', { eventCategory: 'Xem cửa hàng', eventAction: 'Check', eventLabel: 'Check Showroom'});">
					<img class="mini-navi-icon" src="wp-content/themes/nuty-suntorydesign-child/images/location.png">
					<strong class="mini-navi-title">Đăng nhập</strong>
				</a>
			</li>

		</ul>
	</section>

	<div class="popup-quick-shop" style="display: none;">
		<div class="mask"></div>
		<div class="container">
			<div class="quick-shop-close"><span>Đóng</span></div>
		</div>
	</div>


	<a href="#" class="scrollToTop basel-tooltip">Scroll To Top</a>
	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<script type='text/javascript' src='js/photon.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var wpcf7 = { "apiSettings": { "root": "https:\/\/nuty.vn\/wp-json\/contact-form-7\/v1", "namespace": "contact-form-7\/v1" }, "cached": "1" };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/scriptsc721.js?ver=5.1'></script>
	<script type='text/javascript' src='js/devicepx-jetpack58fa.js?ver=201918'></script>
	<script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var wc_add_to_cart_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "i18n_view_cart": "Xem gi\u1ecf h\u00e0ng", "cart_url": "https:\/\/nuty.vn\/gio-hang", "is_cart": "", "cart_redirect_after_add": "no" };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/add-to-cart.min.js'></script>
	<script type='text/javascript' src='js/js.cookie.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var woocommerce_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%" };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/woocommerce.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var wc_cart_fragments_params = { "ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "cart_hash_key": "wc_cart_hash_edadc3f0414a2b131f0703cbe3bd6ecd", "fragment_name": "wc_fragments_edadc3f0414a2b131f0703cbe3bd6ecd" };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/cart-fragments.min.js'></script>
	<script type='text/javascript'>
		jQuery('body').bind('wc_fragments_refreshed', function () {
			jQuery('body').trigger('jetpack-lazy-images-load');
		});

	</script>
	<script type='text/javascript' src='js/lazy-images.min.js'></script>
	<script type='text/javascript' src='js/isotope.pkgd.min268f.js?ver=4.5.0'></script>
	<script type='text/javascript' src='js/libraries268f.js?ver=4.5.0'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var basel_settings = { "adding_to_cart": "Processing", "added_to_cart": "S\u1ea3n ph\u1ea9m \u0111\u00e3 \u0111\u01b0\u1ee3c th\u00eam th\u00e0nh c\u00f4ng v\u00e0o gi\u1ecf h\u00e0ng c\u1ee7a b\u1ea1n.\n", "continue_shopping": "Ti\u1ebfp t\u1ee5c mua h\u00e0ng", "view_cart": "View Cart", "go_to_checkout": "Checkout", "countdown_days": "ng\u00e0y", "countdown_hours": "hr", "countdown_mins": "min", "countdown_sec": "sc", "loading": "Loading...", "close": "Close (Esc)", "share_fb": "Share on Facebook", "pin_it": "Pin it", "tweet": "Tweet", "download_image": "Download image", "wishlist": "no", "cart_url": "https:\/\/nuty.vn\/gio-hang", "ajaxurl": "https:\/\/nuty.vn\/wp-admin\/admin-ajax.php", "add_to_cart_action": "widget", "categories_toggle": "yes", "enable_popup": "no", "popup_delay": "2000", "popup_event": "time", "popup_scroll": "1000", "popup_pages": "0", "promo_popup_hide_mobile": "yes", "product_images_captions": "no", "all_results": "View all results", "product_gallery": { "images_slider": true, "thumbs_slider": { "enabled": true, "position": "bottom", "items": { "desktop": 4, "desktop_small": 3, "tablet": 4, "mobile": 5, "vertical_items": 3 } } }, "zoom_enable": "no", "ajax_scroll": "yes", "ajax_scroll_class": ".main-page-wrapper", "ajax_scroll_offset": "100", "product_slider_auto_height": "no", "product_slider_autoplay": "", "ajax_add_to_cart": "1", "cookies_version": "1", "header_banner_version": "1", "header_banner_close_btn": "1", "header_banner_enabled": "", "promo_version": "1", "pjax_timeout": "5000", "split_nav_fix": "", "shop_filters_close": "no", "sticky_desc_scroll": "1", "quickview_in_popup_fix": "", "one_page_menu_offset": "150" };
		var basel_variation_gallery_data = null;
/* ]]> */
	</script>
	<script type='text/javascript' src='js/functions268f.js?ver=4.5.0'></script>
	<script type='text/javascript' src='js/underscore.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var _wpUtilSettings = { "ajax": { "url": "\/wp-admin\/admin-ajax.php" } };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/wp-util.min.js'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var wc_add_to_cart_variation_params = { "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "i18n_no_matching_variations_text": "R\u1ea5t ti\u1ebfc, kh\u00f4ng c\u00f3 s\u1ea3n ph\u1ea9m n\u00e0o ph\u00f9 h\u1ee3p v\u1edbi l\u1ef1a ch\u1ecdn c\u1ee7a b\u1ea1n. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c.", "i18n_make_a_selection_text": "Ch\u1ecdn c\u00e1c t\u00f9y ch\u1ecdn cho s\u1ea3n ph\u1ea9m tr\u01b0\u1edbc khi cho s\u1ea3n ph\u1ea9m v\u00e0o gi\u1ecf h\u00e0ng c\u1ee7a b\u1ea1n.", "i18n_unavailable_text": "R\u1ea5t ti\u1ebfc, s\u1ea3n ph\u1ea9m n\u00e0y hi\u1ec7n kh\u00f4ng t\u1ed3n t\u1ea1i. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c." };
/* ]]> */
	</script>
	<script type='text/javascript' src='js/add-to-cart-variation.min.js'></script>
	<script type='text/javascript' src='js/wp-embed.min.js'></script>

	<!-- Root element of PhotoSwipe. Must have class pswp. -->
	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

		<!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
		<div class="pswp__bg"></div>

		<!-- Slides wrapper with overflow:hidden. -->
		<div class="pswp__scroll-wrap">

			<!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>

			<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
			<div class="pswp__ui pswp__ui--hidden">

				<div class="pswp__top-bar">

					<!--  Controls are self-explanatory. Order can be changed. -->

					<div class="pswp__counter"></div>

					<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

					<button class="pswp__button pswp__button--share" title="Share"></button>

					<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

					<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

					<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
					<!-- element will get class pswp__preloader--active when preloader is running -->
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>

				<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
				</button>

				<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
				</button>

				<div class="pswp__caption">
					<div class="pswp__caption__center"></div>
				</div>

			</div>

		</div>

	</div>

</body>

</html>