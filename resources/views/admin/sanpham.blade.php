@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
<script src="sourceadmin/js/jquery-3.3.1.js"></script>
<script>
$(function(){ // this will be called when the DOM is ready
  $('#priceadd').keyup(function() {
    var $form = $("#formadd");
    var $input = $form.find("#priceadd");
    var $input1 = $form.find("#promotion_priceadd");

    // When user select text in the document, also abort.
    var selection = window.getSelection().toString();
        if ( selection !== '' ) {
            return;
        }
        
        // When the arrow keys are pressed, abort.
        if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
            return;
        }
        
        
        var $this = $( this );
        
        // Get the value.
        var input = $this.val();
        
        var input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt( input, 10 ) : 0;

            $this.val( function() {
                return ( input === 0 ) ? "0" : input.toLocaleString( "de-DE" );
        } );
  });
});

$(function(){ // this will be called when the DOM is ready
  $('#promotion_priceadd').keyup(function() {
    var $form = $("#formadd");
    var $input = $form.find("#priceadd");
    var $input1 = $form.find("#promotion_priceadd");

    // When user select text in the document, also abort.
    var selection = window.getSelection().toString();
        if ( selection !== '' ) {
            return;
        }
        
        // When the arrow keys are pressed, abort.
        if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
            return;
        }
        
        
        var $this = $( this );
        
        // Get the value.
        var input = $this.val();
        
        var input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt( input, 10 ) : 0;

            $this.val( function() {
                return ( input === 0 ) ? "0" : input.toLocaleString( "de-DE" );
        } );
  });
});
</script>
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Danh sách sản phẩm</h2>
        <a href="{{ route('themsanpham') }}" class="btn btn-danger dt-add" name="themoi">Thêm mới sản phẩm</a>
      </div>
      <table id="sanpham" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tên sản phẩm</th>
             <th>Loại sản phẩm</th>
             <th>Hình ảnh</th>
             {{--  <th>Mô tả</th>  --}}
             <th>Giá gốc</th>
             <th>Giá khuyến mãi</th>
             <th>Sản phẩm mới</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>

        @foreach($product_type_name as $sanpham)
          <tr>
            <th scope="row">{{ $sanpham->id_product }}</th>
             <td>{{ $sanpham->name_product }}</td>
             <td>{{ $sanpham->name_type_product }}</td>
             <td><img src="source/uploads/product/{{ $sanpham->image }}" style="width: 100px; height: 90px;" /></td>
             {{--  <td>{{ $sanpham->short_description_product }}</td>  --}}
             <td>{{ number_format(($sanpham->unit_price), 0, ',', '.') }}đ</td>
             <td>{{ number_format(($sanpham->promotion_price), 0, ',', '.') }}đ</td>
             <td>{{ $sanpham->new == 1 ? "Có" : "Không" }}</td>
          <td><a href="{{ route('suasanpham', $sanpham->id_product) }}" class="sua-san-pham" style="cursor: pointer;">Sửa</a> - <a class="dt-remove" style="cursor: pointer;">Xóa</a></td>
          </tr>
          @endforeach

       </tbody>
      </table>
    </div>

    <!-- Modal -->
    <div id="modalDelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#"></form>
        <form action="{{ route('xoasanpham') }}" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa sản phẩm này</label>
                <input type="hidden" class="form-control" name="idxoa" id="idxoa">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoasp" value="Xóa sản phẩm">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#sanpham').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
    <script>
      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idedit').val(dtRow[0].cells[0].innerHTML);
            $('#tensp').val(dtRow[0].cells[1].innerHTML);
            $('#mota').val(dtRow[0].cells[4].innerHTML);
            var str = dtRow[0].cells[3].innerHTML;
            var str_replace = str.replace('<img src="s', 's');
            var str_replace1 = str_replace.replace('" style="width: 100px; height: 90px;">', '');
            console.log(str_replace1);
            $("#image").attr("src", str_replace1);
            $('#price').val(dtRow[0].cells[5].innerHTML);
            $('#promotion_price').val(dtRow[0].cells[6].innerHTML);
            // $('#noibat').val(dtRow[0].cells[5].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

      //add
      $('.dt-add').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd').modal('show');
        });
      });

       //delete
      $('.dt-remove').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalDelete').modal('show');
        });
      });
    </script>
    <script>
        (function($, undefined) {


    // When ready.
    $(function() {
    
    var $form = $("#form");
    var $input = $form.find("#price");
    var $input1 = $form.find("#promotion_price");

    $input.on( "keyup", function( event ) {
        
        
        // When user select text in the document, also abort.
        var selection = window.getSelection().toString();
        if ( selection !== '' ) {
            return;
        }
        
        // When the arrow keys are pressed, abort.
        if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
            return;
        }
        
        
        var $this = $( this );
        
        // Get the value.
        var input = $this.val();
        
        var input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt( input, 10 ) : 0;

            $this.val( function() {
                return ( input === 0 ) ? "0" : input.toLocaleString( "de-DE" );
            } );
    } );

    $input1.on( "keyup", function( event ) {
        
        
        // When user select text in the document, also abort.
        var selection = window.getSelection().toString();
        if ( selection !== '' ) {
            return;
        }
        
        // When the arrow keys are pressed, abort.
        if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
            return;
        }
        
        
        var $this = $( this );
        
        // Get the value.
        var input1 = $this.val();
        
        var input1 = input1.replace(/[\D\s\._\-]+/g, "");
            input1 = input1 ? parseInt( input1, 10 ) : 0;

            $this.val( function() {
                return ( input1 === 0 ) ? "0" : input1.toLocaleString( "de-DE" );
            } );
    } );
        });
        })(jQuery);
    </script>
@endsection