@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
            <div class="page-header">
                    @if(Session::has('thongbao'))
                      <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
                    @endif
          
                    @if(count($errors) > 0)
                      @foreach($errors->all() as $er)
                          <div class="alert alert-danger">{{ $er }}</div>
                      @endforeach
                    @endif
                  <h2>Danh sách loại sản phẩm</h2>
                  <input type="submit" class="btn btn-success dt-add" name="themmoi" value="Thêm loại sản phẩm">
                </div>
                <table id="sanpham" class="display table table-bordered table-hover">
                 <thead>
                    <tr>
                       <th>ID</th>
                       <th>Tên loại sản phẩm</th>
                       <th>Thao tác</th>
                    </tr>
                 </thead>
                 <tbody>
          
                  @foreach($type_product_all as $tpa)
                    <tr>
                      <th scope="row">{{ $tpa->id }}</th>
                       <td>{{ $tpa->name }}</td>
                       <td><a class="dt-edit" style="cursor: pointer;">Sửa</a> - <a class="dt-remove" style="cursor: pointer;">Xóa</a></td>
                    </tr>
                    @endforeach
          
                 </tbody>
                </table>
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Danh mục con</h2>
        <input type="submit" class="btn btn-success dt-add-1" name="themoicon" value="Thêm danh mục con">
      </div>
      <table id="danhmuccon" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tên danh mục con</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>

        @foreach($subcategory as $st)
          <tr>
            <th scope="row">{{ $st->id }}</th>
             <td>{{ $st->name }}</td>
             <td><a class="dt-edit-con" style="cursor: pointer;">Sửa</a> - <a class="dt-remove-con" style="cursor: pointer;">Xóa</a></td>
          </tr>
          @endforeach

       </tbody>
      </table>
    </div>

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="{{ route('capnhatlsp') }}" method="POST" id="form" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin </h4>
          </div>
          <div class="modal-body">
                <div class="form-group">
                        <input type="hidden" class="form-control" name="ideditlsp" id="ideditlsp">
                        <label for="exampleInputEmail1">Tên loại sản phẩm</label>
                        <input type="text" class="form-control" name="tenlsp" id="tenlsp">
                </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="capnhatlsp" value="Cập nhật">
          </div>
          
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalEdit1" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="{{ route('capnhatlsp') }}" method="POST" id="form" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin </h4>
          </div>
          <div class="modal-body">
                <div class="form-group">
                        <input type="hidden" class="form-control" name="idlspcon" id="idlspcon">
                        <label for="exampleInputEmail1">Tên loại sản phẩm</label>
                        <input type="text" class="form-control" name="tenlspcon" id="tenlspcon">
                </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="capnhatlspcon" value="Cập nhật danh mục con">
          </div>
          
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalEdit2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin </h4>
          </div>
          <div class="modal-body">
                <div class="form-group">
                        <input type="hidden" class="form-control" name="idlslpconn" id="idlslpconn">
                        <label for="exampleInputEmail1">Bạn có chắc chắn muốn xóa danh mục con này?</label>
                </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoadmcon" value="Xóa danh mục con">
          </div>
          
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin loại sản phẩm</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputFile">Tên loại sản phẩm</label>
                <input type="text" class="form-control" name="tenlspadd" id="tenlspadd">
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="themlsp" value="Thêm mới">
          </div>
         
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalAdd1" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin loại sản phẩm</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputFile">Tên loại sản phẩm</label>
                <input type="text" class="form-control" name="tenlspadd1" id="tenlspadd1">
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="themlsp1" value="Thêm mới">
          </div>
         
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalDelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
         
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa loại sản phẩm này</label>
                <input type="hidden" class="form-control" name="idxoalsp" id="idxoalsp">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoalsp" value="Xóa">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#sanpham').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
    <script>
            $(document).ready(function() {
              $('#danhmuccon').DataTable({
                "pagingType": "full_numbers",
                "language": {
                    "search": "Tìm kiếm:",
                }
              });
            });
          </script>
    <script>
      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#ideditlsp').val(dtRow[0].cells[0].innerHTML);
            $('#tenlsp').val(dtRow[0].cells[1].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

      $('.dt-edit-con').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idlspcon').val(dtRow[0].cells[0].innerHTML);
            $('#tenlspcon').val(dtRow[0].cells[1].innerHTML);
          }
          $('#modalEdit1').modal('show');
        });
      });

      $('.dt-remove-con').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idlslpconn').val(dtRow[0].cells[0].innerHTML);
            $('#tenlspconn').val(dtRow[0].cells[1].innerHTML);
          }
          $('#modalEdit2').modal('show');
        });
      });

      //add
      $('.dt-add').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd').modal('show');
        });
      });

      //add
      $('.dt-add-1').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd1').modal('show');
        });
      });

       //delete
      $('.dt-remove').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoalsp').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalDelete').modal('show');
        });
      });

       //delete
       $('.dt-remove-con').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoalspcon').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalDelete1').modal('show');
        });
      });
    </script>
@endsection