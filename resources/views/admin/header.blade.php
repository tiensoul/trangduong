<style>
    .navbar-default .navbar-nav>li>a
    {
        color: #be3110;
    }
</style>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top" style="background-color: #d9edf7; border-color: #a2d0e7;">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('indexAdmin')}}">QUẢN LÝ MỸ PHẨM</a>
        </div>
        @if(Auth::check())
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
        <li><a href="{{ route('cauhinh')}}">Cấu hình</a></li>
            <li><a href="{{ route('sanpham')}}">Sản phẩm</a></li>
            <li><a href="{{ route('slide')}}">Trình chiếu</a></li>
            <li><a href="{{ route('donhang') }}">Đơn hàng</a></li>
            <li><a href="{{ route('khachhang') }}">Khách hàng</a></li>
            <li><a href="{{ route('danhmuc') }}">Danh mục</a></li>
            @if(Auth::check())
                <li><a href="{{ route('dangxuatadmin') }}">Đăng xuất</a></li>
                <li><a href="admin/index">Xin chào, <i>{{ Auth::user()->name }}</i></a></li>
            @else
                <li><a href="{{ route('login') }}">Đăng nhập</a></li>
            @endif
        </ul>
        </div><!--/.nav-collapse -->
        @endif
    </div>
</nav>